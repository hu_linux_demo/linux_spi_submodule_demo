/**
 * @file      : main.c
 * @brief     : 程序入口文件
 * @author    : huenrong (huenrong1028@gmail.com)
 * @date      : 2021-04-03 13:25:50
 * @author    : huenrong
 *
 * @copyright : Copyright (c) 2021 胡恩荣
 *
 */

#include <stdio.h>
#include <unistd.h>

#include "./icm20608/icm20608.h"

/**
 * @brief  : 程序入口
 * @param  : argc: 参数个数
 * @param  : argv: 参数列表
 * @return : 成功: 0
 *           失败: 其它
 */
int main(int argc, char *argv[])
{
    int ret = -1;

    // ICM20608初始化
    ret = icm20608_init();
    if (-1 == ret)
    {
        perror("icm20608 init fail");

        return -1;
    }

    // ICM20608读取设备ID
    uint8_t icm20608_id = 0;
    ret = icm20608_read_id(&icm20608_id);
    if (0 != ret)
    {
        printf("icm20608 read id fail\n");

        return -1;
    }

    printf("icm20608 id: 0x%02X\n", icm20608_id);

    while (1)
    {
        // ICM20608读取加速度值
        float accel_x = 0;
        float accel_y = 0;
        float accel_z = 0;
        ret = icm20608_read_accel(&accel_x, &accel_y, &accel_z);
        if (0 != ret)
        {
            printf("icm20608 read accel fail\n");
        }

        // ICM20608读取温度值
        float temp = 0;
        ret = icm20608_read_temp(&temp);
        if (0 != ret)
        {
            printf("icm20608 read temp fail\n");
        }

        // icm20608读取陀螺仪值
        float gyro_x = 0;
        float gyro_y = 0;
        float gyro_z = 0;
        ret = icm20608_read_gyro(&gyro_x, &gyro_y, &gyro_z);
        if (0 != ret)
        {
            printf("icm20608 read gyro fail\n");
        }

        // 打印各参数
        printf("accel_x = %.2f (g), accel_x = %.2f (g), accel_x = %.2f (g)\n",
               accel_x, accel_y, accel_z);
        printf("gyro_x = %.2f (°/S), gyro_x = %.2f (°/S), gyro_x = %.2f (°/S)\n",
               gyro_x, gyro_y, gyro_z);
        printf("temp = %.2f (°C)\n\n", temp);
        printf("==========================================================\n\n");

        sleep(2);
    }

    icm20608_close();

    return 0;
}
