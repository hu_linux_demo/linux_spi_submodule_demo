/**
 * @file      : icm20608.h
 * @brief     : ICM20608驱动头文件
 * @author    : huenrong (huenrong1028@gmail.com)
 * @date      : 2021-04-03 13:29:59
 * @author    : huenrong
 *
 * @copyright : Copyright (c) 2021 胡恩荣
 *
 */

#ifndef __ICM20608_H
#define __ICM20608_H

#include <stdint.h>

#include "../gpio/gpio.h"
#include "../spi/spi.h"

// ICM20608设备路径
#define ICM20608_DEV_PATH "/dev/spidev2.0"

// ICM20608设备片选脚(GPIO1_IO20)
#define GPIO_ICM20608_CS 20

// ICM20608设备片选脚控制
#define ICM20608_CS_L() gpio_set_value(GPIO_ICM20608_CS, GPIO_LOW)
#define ICM20608_CS_H() gpio_set_value(GPIO_ICM20608_CS, GPIO_HIGH)

// 陀螺仪和加速度自测(出厂时设置, 用于与用户的自检输出值比较)
#define ICM20_SELF_TEST_X_GYRO 0x00
#define ICM20_SELF_TEST_Y_GYRO 0x01
#define ICM20_SELF_TEST_Z_GYRO 0x02
#define ICM20_SELF_TEST_X_ACCEL 0x0D
#define ICM20_SELF_TEST_Y_ACCEL 0x0E
#define ICM20_SELF_TEST_Z_ACCEL 0x0F

// 陀螺仪静态偏移
#define ICM20_XG_OFFS_USRH 0x13
#define ICM20_XG_OFFS_USRL 0x14
#define ICM20_YG_OFFS_USRH 0x15
#define ICM20_YG_OFFS_USRL 0x16
#define ICM20_ZG_OFFS_USRH 0x17
#define ICM20_ZG_OFFS_USRL 0x18

#define ICM20_SMPLRT_DIV 0x19
#define ICM20_CONFIG 0x1A
#define ICM20_GYRO_CONFIG 0x1B
#define ICM20_ACCEL_CONFIG 0x1C
#define ICM20_ACCEL_CONFIG2 0x1D
#define ICM20_LP_MODE_CFG 0x1E
#define ICM20_ACCEL_WOM_THR 0x1F
#define ICM20_FIFO_EN 0x23
#define ICM20_FSYNC_INT 0x36
#define ICM20_INT_PIN_CFG 0x37
#define ICM20_INT_ENABLE 0x38
#define ICM20_INT_STATUS 0x3A

// 加速度输出
#define ICM20_ACCEL_XOUT_H 0x3B
#define ICM20_ACCEL_XOUT_L 0x3C
#define ICM20_ACCEL_YOUT_H 0x3D
#define ICM20_ACCEL_YOUT_L 0x3E
#define ICM20_ACCEL_ZOUT_H 0x3F
#define ICM20_ACCEL_ZOUT_L 0x40

// 温度输出
#define ICM20_TEMP_OUT_H 0x41
#define ICM20_TEMP_OUT_L 0x42

// 陀螺仪输出
#define ICM20_GYRO_XOUT_H 0x43
#define ICM20_GYRO_XOUT_L 0x44
#define ICM20_GYRO_YOUT_H 0x45
#define ICM20_GYRO_YOUT_L 0x46
#define ICM20_GYRO_ZOUT_H 0x47
#define ICM20_GYRO_ZOUT_L 0x48

#define ICM20_SIGNAL_PATH_RESET 0x68
#define ICM20_ACCEL_INTEL_CTRL 0x69
#define ICM20_USER_CTRL 0x6A
#define ICM20_PWR_MGMT_1 0x6B
#define ICM20_PWR_MGMT_2 0x6C
#define ICM20_FIFO_COUNTH 0x72
#define ICM20_FIFO_COUNTL 0x73
#define ICM20_FIFO_R_W 0x74
#define ICM20_WHO_AM_I 0x75

// 加速度静态偏移
#define ICM20_XA_OFFSET_H 0x77
#define ICM20_XA_OFFSET_L 0x78
#define ICM20_YA_OFFSET_H 0x7A
#define ICM20_YA_OFFSET_L 0x7B
#define ICM20_ZA_OFFSET_H 0x7D
#define ICM20_ZA_OFFSET_L 0x7E


/**
 * @brief  ICM20608初始化
 * @return 成功: 0
 *         失败: -1
 */
int icm20608_init(void);

/**
 * @brief  ICM20608关闭设备
 * @return 成功: 0
 *         失败: -1
 */
int icm20608_close(void);

/**
 * @brief  ICM20608读取设备ID
 * @param  read_id: 输出参数, 读取到的设备id
 * @return 成功: 0
 *         失败: -1
 */
int icm20608_read_id(uint8_t *read_id);

/**
 * @brief  ICM20608读取加速度值
 * @param  read_accel_x: 输出参数, 加速度x轴实际数据(单位: g)
 * @param  read_accel_y: 输出参数, 加速度y轴实际数据(单位: g)
 * @param  read_accel_z: 输出参数, 加速度z轴实际数据(单位: g)
 * @return 成功: 0
 *         失败: -1
 */
int icm20608_read_accel(float *read_accel_x,
                        float *read_accel_y,
                        float *read_accel_z);

/**
 * @brief  ICM20608读取温度值
 * @param  read_temp: 输出参数, 温度实际值(单位: °C)
 * @return 成功: 0
 *         失败: -1
 */
int icm20608_read_temp(float *read_temp);

/**
 * @brief  ICM20608读取陀螺仪值
 * @param  read_gyro_x: 输出参数, 陀螺仪x轴实际数据(单位: °/S)
 * @param  read_gyro_y: 输出参数, 陀螺仪y轴实际数据(单位: °/S)
 * @param  read_gyro_z: 输出参数, 陀螺仪z轴实际数据(单位: °/S)
 * @return 成功: 0
 *         失败: -1
 */
int icm20608_read_gyro(float *read_gyro_x,
                       float *read_gyro_y,
                       float *read_gyro_z);

#endif // __ICM20608_H
