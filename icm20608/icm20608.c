/**
 * @file      : icm20608.c
 * @brief     : ICM20608驱动源文件
 * @author    : huenrong (huenrong1028@gmail.com)
 * @date      : 2021-04-03 13:27:35
 * @author    : huenrong
 *
 * @copyright : Copyright (c) 2021 胡恩荣
 *
 */

#include "icm20608.h"

#include <stdio.h>
#include <unistd.h>

/**
 * @brief  ICM20608初始化
 * @return 成功: 0
 *         失败: -1
 */
int icm20608_init(void)
{
    int ret = -1;

    // 打开设备
    ret = spi_open(ICM20608_DEV_PATH, SPI_MODE_0, 8000000, 8);
    if (0 != ret)
    {
        return -1;
    }

    // 导出片选引脚
    ret = gpio_export(GPIO_ICM20608_CS);
    if (0 != ret)
    {
        return -1;
    }

    // 设置片选引脚方向
    ret = gpio_set_direction(GPIO_ICM20608_CS, GPIO_OUT);
    if (0 != ret)
    {
        return -1;
    }

    // 设置片选引脚电平
    ret = gpio_set_value(GPIO_ICM20608_CS, GPIO_LOW);
    if (0 != ret)
    {
        return -1;
    }

    // 寄存器初始化
    ICM20608_CS_L();
    // usleep(1 * 1000);
    // 重置内部寄存器, 并恢复默认设置
    ret = spi_write_byte(ICM20608_DEV_PATH, (ICM20_PWR_MGMT_1 & 0x7F), 0x80);
    ICM20608_CS_H();
    if (0 != ret)
    {
        return -1;
    }
    usleep(50 * 1000);

    // 自动选择最佳的可用时钟源–PLL(如果已准备好), 否则使用内部振荡器
    ICM20608_CS_L();
    ret = spi_write_byte(ICM20608_DEV_PATH, (ICM20_PWR_MGMT_1 & 0x7F), 0x01);
    ICM20608_CS_H();
    if (0 != ret)
    {
        return -1;
    }
    usleep(50 * 1000);

    // 输出速率是内部采样率
    ICM20608_CS_L();
    ret = spi_write_byte(ICM20608_DEV_PATH, (ICM20_SMPLRT_DIV & 0x7F), 0x00);
    ICM20608_CS_H();
    if (0 != ret)
    {
        return -1;
    }

    // 陀螺仪±2000dps量程
    ICM20608_CS_L();
    ret = spi_write_byte(ICM20608_DEV_PATH, (ICM20_GYRO_CONFIG & 0x7F), 0x18);
    ICM20608_CS_H();
    if (0 != ret)
    {
        return -1;
    }

    // 加速度计±16G量程
    ICM20608_CS_L();
    ret = spi_write_byte(ICM20608_DEV_PATH, (ICM20_ACCEL_CONFIG & 0x7F), 0x18);
    ICM20608_CS_H();
    if (0 != ret)
    {
        return -1;
    }

    // 陀螺仪低通滤波BW=20Hz
    ICM20608_CS_L();
    ret = spi_write_byte(ICM20608_DEV_PATH, (ICM20_CONFIG & 0x7F), 0x04);
    ICM20608_CS_H();
    if (0 != ret)
    {
        return -1;
    }

    // 加速度计低通滤波BW=21.2Hz
    ICM20608_CS_L();
    ret = spi_write_byte(ICM20608_DEV_PATH, (ICM20_ACCEL_CONFIG2 & 0x7F), 0x04);
    ICM20608_CS_H();
    if (0 != ret)
    {
        return -1;
    }

    // 打开加速度计和陀螺仪所有轴
    ICM20608_CS_L();
    ret = spi_write_byte(ICM20608_DEV_PATH, (ICM20_PWR_MGMT_2 & 0x7F), 0x00);
    ICM20608_CS_H();
    if (0 != ret)
    {
        return -1;
    }

    // 关闭低功耗
    ICM20608_CS_L();
    ret = spi_write_byte(ICM20608_DEV_PATH, (ICM20_LP_MODE_CFG & 0x7F), 0x00);
    ICM20608_CS_H();
    if (0 != ret)
    {
        return -1;
    }

    // 关闭FIFO
    ICM20608_CS_L();
    ret = spi_write_byte(ICM20608_DEV_PATH, (ICM20_FIFO_EN & 0x7F), 0x00);
    ICM20608_CS_H();
    if (0 != ret)
    {
        return -1;
    }

    return 0;
}

/**
 * @brief  ICM20608关闭设备
 * @return 成功: 0
 *         失败: -1
 */
int icm20608_close(void)
{
    int ret = -1;

    ret = spi_close(ICM20608_DEV_PATH);

    return ret;
}

/**
 * @brief  ICM20608读取设备ID
 * @param  read_id: 输出参数, 读取到的设备id
 * @return 成功: 0
 *         失败: -1
 */
int icm20608_read_id(uint8_t *read_id)
{
    int ret = -1;

    // 读取设备id
    ICM20608_CS_L();
    ret = spi_read_byte(read_id, ICM20608_DEV_PATH, (ICM20_WHO_AM_I | 0x80));
    ICM20608_CS_H();
    // 读取失败
    if (0 != ret)
    {
        return -1;
    }

    return 0;
}

/**
 * @brief  ICM20608读取加速度值
 * @param  read_accel_x: 输出参数, 加速度x轴实际数据(单位: g)
 * @param  read_accel_y: 输出参数, 加速度y轴实际数据(单位: g)
 * @param  read_accel_z: 输出参数, 加速度z轴实际数据(单位: g)
 * @return 成功: 0
 *         失败: -1
 */
int icm20608_read_accel(float *read_accel_x,
                        float *read_accel_y,
                        float *read_accel_z)
{
    int ret = -1;
    uint8_t read_data[6] = {0};

    // 读取加速度数据
    ICM20608_CS_L();
    usleep(50 * 1000);
    ret = spi_read_nbyte(read_data, ICM20608_DEV_PATH,
                         (ICM20_ACCEL_XOUT_H | 0x80), 6);
    ICM20608_CS_H();
    // 读取失败
    if (0 != ret)
    {
        return -1;
    }

    // 原始值
    *read_accel_x = (short)((read_data[0] << 8) | read_data[1]);
    *read_accel_y = (short)((read_data[2] << 8) | read_data[3]);
    *read_accel_z = (short)((read_data[4] << 8) | read_data[5]);

    // 原始值转实际值
    *read_accel_x /= 2048.0;
    *read_accel_y /= 2048.0;
    *read_accel_z /= 2048.0;

    return 0;
}

/**
 * @brief  ICM20608读取温度值
 * @param  read_temp: 输出参数, 温度实际值(单位: °C)
 * @return 成功: 0
 *         失败: -1
 */
int icm20608_read_temp(float *read_temp)
{
    int ret = -1;
    uint8_t read_data[2] = {0};

    // 读取温度
    ICM20608_CS_L();
    ret = spi_read_nbyte(read_data, ICM20608_DEV_PATH,
                         (ICM20_TEMP_OUT_H | 0x80), 2);
    ICM20608_CS_H();
    // 读取失败
    if (0 != ret)
    {
        return -1;
    }

    // 原始值
    *read_temp = (short)((read_data[0] << 8) | read_data[1]);

    // 原始值转实际值
    *read_temp = (((*read_temp - 25) / 326.8) + 25);

    return 0;
}

/**
 * @brief  ICM20608读取陀螺仪值
 * @param  read_gyro_x: 输出参数, 陀螺仪x轴实际数据(单位: °/S)
 * @param  read_gyro_y: 输出参数, 陀螺仪y轴实际数据(单位: °/S)
 * @param  read_gyro_z: 输出参数, 陀螺仪z轴实际数据(单位: °/S)
 * @return 成功: 0
 *         失败: -1
 */
int icm20608_read_gyro(float *read_gyro_x,
                       float *read_gyro_y,
                       float *read_gyro_z)
{
    int ret = -1;
    uint8_t read_data[6] = {0};

    // 读取陀螺仪数据
    ICM20608_CS_L();
    ret = spi_read_nbyte(read_data, ICM20608_DEV_PATH,
                         (ICM20_GYRO_XOUT_H | 0x80), 6);
    ICM20608_CS_H();
    // 读取失败
    if (0 != ret)
    {
        return -1;
    }

    // 原始值
    *read_gyro_x = (short)((read_data[0] << 8) | read_data[1]);
    *read_gyro_y = (short)((read_data[2] << 8) | read_data[3]);
    *read_gyro_z = (short)((read_data[4] << 8) | read_data[5]);

    // 原始值转实际值
    *read_gyro_x /= 16.4;
    *read_gyro_y /= 16.4;
    *read_gyro_z /= 16.4;

    return 0;
}
